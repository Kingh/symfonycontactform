<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class ContactController extends AbstractController
{
    public function index(Request $request, \Swift_Mailer $mailer)
    {
        $content = new Contact();
        $form = $this->createForm(ContactType::Class, $content);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $formData = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($formData);
            $entityManager->flush();
            $message = (new \Swift_Message(' You received mail'))
                ->setFrom($content->getSender())
                ->setTo('mohlala.tumelo@gmail.com')
                ->setBody(
                    $content->getMessage(),
                    'text/plain'
                );

            $mailer->send($message);
        }

        return $this->render('contact/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
